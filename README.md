<img align="right" src="figures/rwth_ient_logo@2x.png" alt="Logo Institut für Nachrichtentechnik | RWTH Aachen University" width="240px">

# ient.nb

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2FIENT%2Fient.nb/master?urlpath=lab/tree/index.ipynb)

Functionality to enhance Jupyter Notebooks IENT style

## Contents

Visit the notebook [index.ipynb](index.ipynb) for a table of contents.

* `ient_plots.py` offers to style Matplotlib axes the IENT way (e.g. with arrows below the axes) as well as the RWTH colors
* `ient_signals.py` consists of common signal definitions in NumPy
* `ient_filters.py` and `ient_transforms.py` hold definitions of certain FIR/IIR filters and transforms respectively
* `ient_tikzmagic.py` defines a magic command to compile TikZ within a notebook (forked from https://github.com/mkrphys/ipython-tikzmagic)

## Usage

* Run the interactive notebooks directly online with binder:

  [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgit.rwth-aachen.de%2FIENT%2Fient.nb/master?urlpath=lab/tree/index.ipynb)
* Clone (or download as Zip) this repository with `git clone --recurse-submodules` and open the notebooks locally on your PC with Jupyter ([Anaconda](https://www.anaconda.com/) is highly recommended for this).
* To run JupyterLab with ient.nb in a Docker Container directly, run

  ```bash
  docker run --name='jt' --rm -it -p 8888:8888 -e JUPYTER_ENABLE_LAB=yes registry.git.rwth-aachen.de/ient/ient.nb:master
  ```