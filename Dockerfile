ARG BASE_IMAGE=registry.git.rwth-aachen.de/acs/cloud/jupyter/singleuser/python:latest
FROM ${BASE_IMAGE}

# update conda base environment to match specifications in environment.yml
ADD binder/environment.yml /tmp/environment.yml
USER root
RUN sed -i "s|name\: ientlab|name\: base|g" /tmp/environment.yml # we need to replace the name of the environment with base such that we can update the base environment here
USER $NB_USER
RUN cat /tmp/environment.yml
RUN conda env update -f /tmp/environment.yml

# cleanup conda packages
RUN conda clean --all -f -y

# install some extensions defined in binder postBuild
ADD binder/postBuild /tmp/postBuild.sh
USER root
RUN chmod +x /tmp/postBuild.sh
USER $NB_USER
RUN /tmp/postBuild.sh

# Copy workspace
COPY ./ /home/jovyan
